Booth toolchains need to be installed to /opt since all the search paths for gcc
and binutils are coded that way. (/opt/eco32-unknown-linux-musl and /opt/eco32-elf)

The Baremetal toolchain requires the linux toolchain to be installed as well.